package ictgradschool.web.lab13.ex01;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by vwen239 on 10/01/2018.
 */
public class ImageGalleryDisplay extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext servletContext = getServletContext();

        String fullPhotoPath = servletContext.getRealPath("/Photos");
        PrintWriter out = resp.getWriter();
        File myDirectory = new File(fullPhotoPath);

        if (!myDirectory.exists()) {
            out.println("<html>\n<head><title>Server Response</title>");
            out.println("</head>\n<body>");
            out.println("Photo Directory Incorrect");
            out.println("</body></html>");

        } else {
            File[] fileList = myDirectory.listFiles();
            System.out.println(fileList);
            String[] fileNames = myDirectory.list();

            out.println("<html>\n<head><title>Server response</title>");
            out.println("</head>\n<body>");
            out.println("<h1>Image Thumbnails</h1>");

            for (int i = 0; i < fileNames.length; i++) {

                String file = fileNames[i];
                if (file.contains("_thumbnail.png")) {
                    String fullPath = file.replace("_thumbnail.png", ".jpg");
                    String fileName = fullPath.replace("_", " ");
                    fileName.replace(".jpg", "");
                    File actualFile = fileList[i];
                    out.println("<a href =\"" + "..\\Photos\\" + fullPath + "\">" + "<img src =" + "\"..\\Photos\\" + file + "\"/></a>");
                    out.println("File Name:" + fileName);
                    out.println("File size: " + actualFile.length() / 1024 + " kb");
                    out.println("<br>");
                }
            }

            out.println("</body></html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
